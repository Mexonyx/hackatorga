package fr.siocoliniere.bo;

import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class HackathonAddJuryMemberTest {
    private Hackathon hacka ;

    @BeforeEach
    void init(){
        hacka = new Hackathon(1,"name 1","topic 1", "desc1");

    }
    @org.junit.jupiter.api.Test
    void testAddJuryMember() {
        Jury member = new Jury(1,"lastname","fisrtname");
        hacka.addJuryMember(member);

        assertTrue(hacka.getJuryMembers().contains(member),"Erreur : un nouveau membre ajouté ");
    }

}