CREATE TABLE hackathon
(
    id          serial  NOT NULL,
    name        varchar NOT NULL UNIQUE,
    topic       text    NOT NULL,
    description text    NOT NULL,

    CONSTRAINT hackathon_pkey PRIMARY KEY (id)
);

CREATE TABLE jury
(
    id        serial      NOT NULL,
    firstname varchar(20) NOT NULL,
    lastname  varchar(20) NULL,
    CONSTRAINT jury_pkey PRIMARY KEY (id)

);

CREATE TABLE jurycompose
(
    idhackathon integer NOT NULL,
    idjury      integer NOT NULL,
    CONSTRAINT jurycompose_pkey PRIMARY KEY (idhackathon, idjury)

);
ALTER TABLE jurycompose
    ADD CONSTRAINT jurycompose_fk_jury FOREIGN KEY (idjury) REFERENCES jury (id);
ALTER TABLE jurycompose
    ADD CONSTRAINT jurycompose_fk_hackathon FOREIGN KEY (idhackathon) REFERENCES hackathon (id);


CREATE TABLE registration
(
    id          serial  NOT NULL,
    idhackathon integer NOT NULL,
    lastname    varchar NOT NULL,
    firstname   varchar NOT NULL,
    CONSTRAINT registration_pk PRIMARY KEY (id)
);

ALTER TABLE registration
    ADD CONSTRAINT registration_fk_hackathon FOREIGN KEY (idhackathon) REFERENCES hackathon (id);