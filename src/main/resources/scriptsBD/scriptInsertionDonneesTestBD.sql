insert into hackathon(name, topic, description)
values ('HackAct', 'Citoyenneté Mondiale',
        'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes, doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.');
insert into hackathon(name, topic, description)
values ('HackInnov', 'Services publics et Innovation',
        'Ce hackathon vise à réunir les citoyens d’une communauté pour développer des solutions afin d’améliorer les services publics offerts aux citoyens');
insert into hackathon(name, topic, description)
values ('HackConsul', 'Réinventer un métier',
        'Ce hackathon vise à réunir des étudiants afin de réinventer le métier de Consultant');

insert into jury(firstname, lastname)
values ('Mathieu', 'Grisse'),
       ('Sara', 'Brucker'),
       ('Florence', 'Markus'),
       ('Thomas', 'Pasque'),
       ('Marc', 'Tombour'),
       ('Cindy', 'Akbou'),
       ('Mathilde', 'Antas'),
       ('Teddy', 'Martin');

insert into jurycompose
values (1, 1),
       (1, 2),
       (1, 3),
       (2, 4),
       (2, 5),
       (3, 6),
       (3, 7),
       (3, 8);

INSERT INTO registration(id, idhackathon, lastname, firstname)
VALUES (1, 1, 'Lourau', 'Julien'),
       (2, 1, 'Haden', 'Charlie');
