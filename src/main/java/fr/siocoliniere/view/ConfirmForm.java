package fr.siocoliniere.view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class ConfirmForm extends JDialog {

    // reponse donnée par l'utilisateur
    private boolean answer;

    /**
     * Constructeur
     * Instancie une fenêtre de dialogue
     * @param title : titre de la fenetre
     * @param message : message à afficher
     */
    public ConfirmForm(Window owner, String title, String message) {
        super(owner);
        this.answer = false;

        initComponents();
        setTitle(title);
        setModal(true);
        setLocation(500, 500);
        getRootPane().setDefaultButton(okButton);
        questionLabel.setText(message);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        // call onCancel() on ESCAPE
        contentPanel.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                noButtonActionPerformed(e);
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * Permet de récupérer la réponse donnée par l'utilisateur
     * @return true pour "yes", false pour "no"
     */
    public boolean isAnswer() {
        return answer;
    }

    /**
     * Setter
     * @param answer true ou false selon la réponse de l'utilisateur
     */
    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

    /**
     * Réponse Yes
     * Appelée : lors du clic sur le bouton Yes
     */
    private void yesButtonActionPerformed(ActionEvent e) {
        setAnswer(true);
        dispose();
    }

    /**
     * Réponse No
     * Appelée : lors du clic sur le bouton No
     */
    private void noButtonActionPerformed(ActionEvent e) {
        setAnswer(false);
        dispose();
    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        questionLabel = new JLabel();
        buttonBar = new JPanel();
        okButton = new JButton();
        noButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

                //---- questionLabel ----
                questionLabel.setText("Message");
                contentPanel.add(questionLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.NONE,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {85, 80};

                //---- okButton ----
                okButton.setText("Yes");
                okButton.addActionListener(e -> yesButtonActionPerformed(e));
                buttonBar.add(okButton, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- noButton ----
                noButton.setText("No");
                noButton.addActionListener(e -> noButtonActionPerformed(e));
                buttonBar.add(noButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel questionLabel;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton noButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
