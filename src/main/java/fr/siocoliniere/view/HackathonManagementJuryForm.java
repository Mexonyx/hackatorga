package fr.siocoliniere.view;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.JuryController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Jury;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;

public class HackathonManagementJuryForm extends JDialog {

    private Hackathon hackathonManaged;

    public HackathonManagementJuryForm(Window owner) {
        super(owner);
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));

        setTitle("Hackat'Orga : Jury ");

        initComponents();

        //initialisation de la JList des jury potentiels
        initPotentialJuryList(JuryController.getInstance().getAll());
    }

    /**
     * Permet d'initialiser la vue pour un hacakthon choisi
     * @param hackathonManaged
     */
    public void initHackathonManaged(Hackathon hackathonManaged){
        this.hackathonManaged = hackathonManaged;
        if (this.hackathonManaged != null) {
            // initialisation de la JList des jury existants
            initJuryList(hackathonManaged.getJuryMembers());
        }
    }

    /**
     * Permet de valoriser la Jlist des membres actuels du hackathon
     * @param juryList liste des membres du jury
     */
    private void initJuryList(java.util.List<Jury> juryList) {
        //!!! Info technique : un objet de type JList fonctionne avec un objet de type DefaultListModel qui va contenir les données

        //création du DefaultListModel à partir de la liste d'objets de type Jury
        DefaultListModel<Jury> modelJuryList = new DefaultListModel<>();
        for (Jury oneJury : juryList) {
            modelJuryList.addElement(oneJury);
        }
        //rattachement du DefaultListModel au composant graphique JList
        this.juryJList.setModel(modelJuryList);

    }

    /**
     * Permet de valoriser la liste des jury potentiels
     * @param juries liste de jury
     */
    private void initPotentialJuryList(List<Jury> juries) {
        //!!! Info technique : un objet de type JList fonctionne avec un objet de type DefaultListModel qui va contenir les données

        //création du DefaultListModel à partir de la liste d'objets de type Jury
        DefaultListModel<Jury> modelPotentialJuryList = new DefaultListModel<>();
        for (Jury oneJury : juries) {
            modelPotentialJuryList.addElement(oneJury);
        }
        //rattachement du DefaultListModel au composant graphique JList
        this.potentialJuryJList.setModel(modelPotentialJuryList);

    }

    /**
     * METHODES RATTACHEES A LA GESTION AUX EVENEMENTS
     */

    /**
     * Permet de retirer le jury sélectionné de la liste du jury
     * Appelée : lors du clic sur le bouton Remove
     */
    private void removeButtonActionPerformed(ActionEvent e) {
        //récupération du jury sélectionné
        Jury jurySelected = (Jury) juryJList.getSelectedValue();

        //suppression du model rattaché à la Jlist gérant le jury
        DefaultListModel<Jury> modelJuryList = (DefaultListModel<Jury>) juryJList.getModel();
        modelJuryList.removeElement(jurySelected);

        //ajout au model rattacha à la JList gérnat les jurys potentiels
        DefaultListModel<Jury> modelMemberList = (DefaultListModel<Jury>) potentialJuryJList.getModel();
        modelMemberList.addElement(jurySelected);
    }

    /**
     * Permet d'ajouter le jury sélectionné de la liste du jury
     * Appelée : lors du clic sur le bouton Add
     */
    private void addButtonActionPerformed(ActionEvent e) {
        // !!!!!! TO IMPLEMENT
        System.out.println("A implémenter : ajout du membre");
    }

    /**
     * Permet de sauvegarder la liste de jury du hackathon
     * Appelée : lors du clic sur le bouton Save
     */
    private void SaveButtonActionPerformed(ActionEvent e) {

        // Construction de la liste de jury à sauvegarder
        List<Jury> juries = new ArrayList<>();

        for (int i =0; i< juryJList.getModel().getSize(); i++){
            Jury jury = (Jury) juryJList.getModel().getElementAt(i);
            juries.add(jury);
        }

        HackathonController.getInstance().saveJuriesByIdHackathon(hackathonManaged,juries);
        this.dispose();
    }

    /**
     * Appelée : lors du clic sur le bouton Cancel
     */
    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        existingLabel = new JLabel();
        potentialLabel = new JLabel();
        juryMemberScrollPane = new JScrollPane();
        juryJList = new JList();
        ARButtonPanel = new JPanel();
        addButton = new JButton();
        removeButton = new JButton();
        memberScrollPane = new JScrollPane();
        potentialJuryJList = new JList();
        buttonBar = new JPanel();
        SaveButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {35, 112, 0, 119, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 1.0, 0.0, 1.0E-4};

                //---- existingLabel ----
                existingLabel.setText("Existing");
                contentPanel.add(existingLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- potentialLabel ----
                potentialLabel.setText("Potential");
                contentPanel.add(potentialLabel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //======== juryMemberScrollPane ========
                {
                    juryMemberScrollPane.setViewportView(juryJList);
                }
                contentPanel.add(juryMemberScrollPane, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(5, 5, 5, 10), 0, 0));

                //======== ARButtonPanel ========
                {
                    ARButtonPanel.setLayout(new GridBagLayout());
                    ((GridBagLayout)ARButtonPanel.getLayout()).columnWidths = new int[] {0, 0};
                    ((GridBagLayout)ARButtonPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
                    ((GridBagLayout)ARButtonPanel.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
                    ((GridBagLayout)ARButtonPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0, 0.0, 1.0, 1.0E-4};

                    //---- addButton ----
                    addButton.setIcon(new ImageIcon(getClass().getResource("/arrowG.png")));
                    addButton.setText("Add");
                    addButton.addActionListener(e -> addButtonActionPerformed(e));
                    ARButtonPanel.add(addButton, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 0), 0, 0));

                    //---- removeButton ----
                    removeButton.setIcon(new ImageIcon(getClass().getResource("/arrowD.png")));
                    removeButton.setText("Remove");
                    removeButton.addActionListener(e -> removeButtonActionPerformed(e));
                    ARButtonPanel.add(removeButton, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                        GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 0), 0, 0));
                }
                contentPanel.add(ARButtonPanel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //======== memberScrollPane ========
                {
                    memberScrollPane.setViewportView(potentialJuryJList);
                }
                contentPanel.add(memberScrollPane, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- SaveButton ----
                SaveButton.setText("Save");
                SaveButton.addActionListener(e -> SaveButtonActionPerformed(e));
                buttonBar.add(SaveButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel existingLabel;
    private JLabel potentialLabel;
    private JScrollPane juryMemberScrollPane;
    private JList juryJList;
    private JPanel ARButtonPanel;
    private JButton addButton;
    private JButton removeButton;
    private JScrollPane memberScrollPane;
    private JList potentialJuryJList;
    private JPanel buttonBar;
    private JButton SaveButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
