package fr.siocoliniere.view;

import fr.siocoliniere.bo.Hackathon;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class HackathonManagementForm extends JDialog {

    private Hackathon hackathonManaged;

    // vues liées
    private HackathonManagementSettingsForm viewHackathonManageSettings;
    private HackathonManagementJuryForm viewHackathonManageJury;

    public HackathonManagementForm() {

        this.hackathonManaged = null;

        setPreferredSize(new Dimension(300, 300));
        setLocation(500, 500);
        setModal(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        pack();
        initComponents();

        // instanciation des vues liées : Settings / Jury
        viewHackathonManageSettings = new HackathonManagementSettingsForm(this);
        viewHackathonManageJury = new HackathonManagementJuryForm(this);

        //masquage des vues liées
        viewHackathonManageSettings.setVisible(false);
        viewHackathonManageJury.setVisible(false);
    }

    /**
     * Permet de valoriser le hackathon à configurer
     * @param hackathon
     */
    public void setHackathon(Hackathon hackathon) {
        this.hackathonManaged = hackathon;
        if (this.hackathonManaged != null){
            setTitle("Hackat'Orga : " + this.hackathonManaged.getName());
        }else {
            setTitle("Hackat'Orga : New hackathon");
        }

    }

    /**
     * METHODES RATTACHEES A LA GESTION AUX EVENEMENTS
     */

    /**
     * Permet de quitter l'application
     * Appelée : lors du clic sur le bouton Close
     */
    private void closeButtonActionPerformed(ActionEvent e) {
        this.setVisible(false);
    }

    /**
     * Permet de paramétrer les settings du Hackathon
     * Appelée : lors du clic sur le bouton Settings
     */
    private void settingsButtonActionPerformed(ActionEvent e) {
        //affichage de la vue pour le hackathon concerné
        viewHackathonManageSettings.initHackathonManaged(hackathonManaged);
        viewHackathonManageSettings.setVisible(true);
    }

    /**
     * Permet de paramétrer le jury du Hackathon
     * Appelée : lors du clic sur le bouton Jury
     */
    private void juryButtonActionPerformed(ActionEvent e) {
        //affichage de la vue pour le hackathon concerné
        viewHackathonManageJury.initHackathonManaged(hackathonManaged);
        viewHackathonManageJury.setVisible(true);
    }


    /**
     * JFormDesigner - DO NOT MODIFY
     */

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        settingsButton = new JButton();
        juryButton = new JButton();
        buttonBar = new JPanel();
        closeButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0, 0.0, 1.0, 1.0E-4};

                //---- settingsButton ----
                settingsButton.setText("Settings");
                settingsButton.addActionListener(e -> settingsButtonActionPerformed(e));
                contentPanel.add(settingsButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- juryButton ----
                juryButton.setText("Jury");
                juryButton.addActionListener(e -> juryButtonActionPerformed(e));
                contentPanel.add(juryButton, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0};

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.addActionListener(e -> closeButtonActionPerformed(e));
                buttonBar.add(closeButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JButton settingsButton;
    private JButton juryButton;
    private JPanel buttonBar;
    private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
