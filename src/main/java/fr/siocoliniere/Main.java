package fr.siocoliniere;

import fr.siocoliniere.view.WelcomeForm;

public class Main {

    public static void main(String[] args) {

        System.out.println("Lancement de l'application");

        WelcomeForm welcomeView = new WelcomeForm();
        welcomeView.setVisible(true);

    }
}
