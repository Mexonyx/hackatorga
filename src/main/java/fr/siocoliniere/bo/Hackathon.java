package fr.siocoliniere.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe métier Hackathon
 * Permet l'instanciation d'un hackathon
 */
public class Hackathon {

    private int id;

    private String name;
    private String topic;
    private String description;

    private List<Jury> juryMembers; // liste des membres du jury du hackathon

    /**
     * Constructeur
     * @param topic topic du hackathon
     * @param desc descprition du hackathon
     */
    public Hackathon(String name,String topic, String desc) {
        this.id = 0;
        this.name = name;
        this.topic = topic;
        this.description = desc;

        this.juryMembers = new ArrayList<>();
    }

    /**
     * Contructeur
     * @param id identifiant du hackathon
     * @param topic topic du hackathon
     * @param desc descprition du hackathon
     */
    public Hackathon(int id, String name, String topic, String desc) {
        this(name, topic, desc);
        this.id = id;

    }

    public int getId() { return id; }
    public String getName() {return name;}
    public String getTopic() {
        return topic;
    }
    public String getDescription() {
        return description;
    }
    public List<Jury> getJuryMembers() {
        return juryMembers;
    }

    public void setId(int id) {this.id = id;}
    public void setName(String name) {this.name = name;}
    public void setTopic(String topic) {
        this.topic = topic;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setJuryMembers(List<Jury> juryMembers) {
        this.juryMembers = juryMembers;
    }

    /**
     * Permet l'ajout d'un membre au jury du hackathon
     * @param member nouveau membre
     */
    public void addJuryMember(Jury member){
        juryMembers.add(member);
    }

    @Override
    public String toString() {
        return this.name + " - " + this.topic;
    }

    /**
     * Permet de vider la liste de membres composant le jury
     */
    public void clearJury() {
        this.juryMembers.clear();

    }
}
