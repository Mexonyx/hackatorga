package fr.siocoliniere.bo;

/**
 * Classe métier JuryMember
 * Permet l'instanciation d'un membre de jury
 */
public class Jury {

    private int id;
    private String lastname;
    private String firstname;

    /**
     * Constructeur
     * @param lastname nom du membre
     * @param firstname prenom du membre
     */
    public Jury(int id, String lastname, String firstname) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return  lastname + ' ' + firstname ;
    }
}
