package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Jury;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DAOJury
 * Implémente la classe abstraite DAO
 * BD : Table Jury
 */
public class DAOJury extends DAO<Jury> {

    /**
     * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
     */
    private static DAOJury instanceDAOJury;
    /**
     * Pattern Singleton
     * @return DAOHackathon
     */
    public static synchronized DAOJury getInstance() {
        if (instanceDAOJury == null) {
            instanceDAOJury = new DAOJury();
        }
        return instanceDAOJury;
    }

    /**
     * REQUETES
     */
    private static final String sqlSelectAllByIdHackathon = "select jury.id,firstname, lastname from jury inner join jurycompose on jury.id = jurycompose.idjury where idhackathon = ?";
    private static final String sqlSelectAllJury = "select id, firstname, lastname from jury";

    /**
     * Permet de récupérer tous les jury
     * @return une liste des jury
     * @throws DALException
     */
    @Override
    public List<Jury> getAll() throws DALException {

        List<Jury> juries = new ArrayList<>();

        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllJury);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                Jury jury = new Jury(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"));
                juries.add(jury);
            }

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }

        return juries;
    }

    /**
     * Permet de récupérer tous les membres participant au jury du hackathon dont l'id est passé en paramètre
     * @return la liste des membres de jurys
     * @throws DALException
     */
    public List<Jury> getAllByIdHackathon(int idHackathon) throws DALException {

        List<Jury> juries = new ArrayList<>();

        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllByIdHackathon);
            rqt.setInt(1, idHackathon);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                Jury jury = new Jury(rs.getInt("id"),rs.getString("lastname"), rs.getString("firstname"));
                juries.add(jury);
            }

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }

        return juries;
    }


    @Override
    public void save(Jury obj) throws DALException {
        // !!!!!!!!!!!!! TO IMPLEMENT
    }

    @Override
    public void update(Jury obj) throws DALException {
        // !!!!!!!!!!!!! TO IMPLEMENT
    }

    @Override
    public Jury getOneById(int id) throws DALException {
        // !!!!!!!!!!!!! TO IMPLEMENT
        return null;
    }

}
