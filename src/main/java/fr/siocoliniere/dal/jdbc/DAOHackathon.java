package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Jury;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DAOHackathon
 * Implémente la classe abstraite DAO
 * BD : Table Hackathon
 */
public class DAOHackathon extends DAO<Hackathon> {

    /**
     * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
     */
    private static DAOHackathon instanceDAOHackathon;
    private Hackathon obj;

    /**
     * Pattern Singleton
     * @return DAOHackathon
     */
    public static synchronized DAOHackathon getInstance() {
        if (instanceDAOHackathon == null) {
            instanceDAOHackathon = new DAOHackathon();
        }
        return instanceDAOHackathon;
    }

    /**
     * REQUETES
     */
    private static final String sqlSelectIdByName= "select hackathon.id from hackathon where hackathon.name = ?";
    private static final String sqlSelectAll = "select hackathon.id, hackathon.name, hackathon.topic, hackathon.description from hackathon";
    private static final String sqlSelectOneById = "select hackathon.id, hackathon.name, hackathon.topic, hackathon.description from hackathon where hackathon.id = ?";

    private static final String sqlUpdateOne = "update hackathon set name = ?, topic = ?, description = ? where id = ?";
    private static  final String sqlInsertOne = "insert into hackathon(name,topic, description) values (?,?,?)";
    private static final String sqlInsertJuryCompose = "insert into jurycompose values (?,?)";
    private static final String sqlDeleteAllComposeJury = "delete from jurycompose where idhackathon = ?";


    /**
     * Permet de supprimer la composition du jury du hackathon
     * @param id : id du hackathon
     * @throws DALException
     */
    public void deleteAllJuryComposeOfHackathon(int id) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteAllComposeJury);
            rqt.setInt(1, id);
            rqt.executeUpdate();
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
    }
    /**
     * Permet de récupérer tous les hackathons stockés dans la table Hackathon
     * @return la liste des hackathons
     * @throws DALException
     */
    @Override
    public List<Hackathon> getAll() throws DALException {

        List<Hackathon> lstHackathon = new ArrayList<>();

        List<Jury> lstMembers;
        try {
            Connection cnx = JdbcTools.getConnection();
            Statement rqt = cnx.createStatement();
            ResultSet rs = rqt.executeQuery(sqlSelectAll);

            while (rs.next()) {
                //instanciation du hackathon
                Hackathon hacka = new Hackathon(rs.getInt("id"),rs.getString("name"),rs.getString("topic"), rs.getString("description"));

                //récupération des membres par appel au DAO gérant les Jury
                lstMembers = DAOJury.getInstance().getAllByIdHackathon(hacka.getId());
                hacka.setJuryMembers(lstMembers);
                lstHackathon.add(hacka);
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }

        return lstHackathon;
    }


    /**
     * Permet de récupérer l'id d'un hackathon connaissant le nom (contrainte d'unicité sur le nom)
     * @param name : nom du hackathon recherché
     * @return id du hackathon
     * @throws DALException
     */
    public int getIdByName(String name) throws DALException {

        int id = 0;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectIdByName);
            rqt.setString(1,name);
            ResultSet rs = rqt.executeQuery();
            while (rs.next()) {
                //instanciation du hackathon
                id=rs.getInt("id");
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
        return id ;
    }


    /**
     * Permet de récupérer un hackathon à partir de son id
     * @param id du hackathon
     * @return le hackathon dont l'id est transmis en paramètre
     * @throws DALException
     */
    @Override
    public Hackathon getOneById(int id) throws DALException {
        Hackathon  hackat=null;

        List<Jury> lstMembers;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
            rqt.setInt(1, id);

            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                hackat = new Hackathon(rs.getInt("id"),rs.getString("name"),rs.getString("topic"), rs.getString("description"));

                //récupération des membres par appel au DAO gérant les Jury
                lstMembers = DAOJury.getInstance().getAllByIdHackathon(hackat.getId());
                hackat.setJuryMembers(lstMembers);
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
        return hackat;
    }


    /**
     * Permet l'insertion d'un nouvel hackathon
     * @param obj : hackathon à inserer
     * @throws DALException
     */
    public void saveSettings(Hackathon obj) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertOne);
            rqt.setString(1,obj.getName());
            rqt.setString(2, obj.getTopic());
            rqt.setString(3, obj.getDescription());
            rqt.executeUpdate();
            rqt.close();

            //mise à jour de l'id du hackathon nouvellement créé - le champ name est UNIQUE
            obj.setId(this.getIdByName(obj.getName()));

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
    }

    

    /**
     * Permet de mettre à jour la composition du jury du hackathon
     * @param idHackathon : hackathon auquel le jury participe
     * @throws DALException
     */
    public void saveJury(int idHackathon,Jury oneJury) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertJuryCompose);
            rqt.setInt(1, idHackathon);
            rqt.setInt(2, oneJury.getId());
            rqt.executeUpdate();
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
    }

    /**
     * Permet de mettre à jour uniquement les settings du hackathon dans la base de données
     * @param obj : hackathon à mettre à jour
     * @throws DALException
     */
    public void updateSettings(Hackathon obj) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();

            PreparedStatement rqt = cnx.prepareStatement(sqlUpdateOne);
            rqt.setString(1,obj.getName());
            rqt.setString(2, obj.getTopic());
            rqt.setString(3, obj.getDescription());
            rqt.setInt(4, obj.getId());
            rqt.executeUpdate();
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
    }


    @Override
    public void save(Hackathon obj) throws DALException {

    }

    @Override
    public void update(Hackathon obj) throws DALException {

    }
}
