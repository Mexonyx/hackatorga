package fr.siocoliniere.bll;


import fr.siocoliniere.bo.Jury;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOHackathon;
import fr.siocoliniere.dal.jdbc.DAOJury;

import java.util.List;

public class JuryController {

    /**
     * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
     */
    private static JuryController instanceCtrl;

    /**
     * Pattern Singleton
     * @return JuryMemberController
     */
    public static synchronized JuryController getInstance(){
        if(instanceCtrl == null){
            instanceCtrl = new JuryController();
        }
        return instanceCtrl;
    }

    /**
     * CONSTRUCTEUR
     */
    private JuryController() {
    }
    /**
     * Permet de récupérer l'ensemble des hackathons géré par la couche de persistance
     * @return la liste des hackathons
     */
    public List<Jury> getAll(){
        List<Jury> juries = null;
        try {
            juries = DAOJury.getInstance().getAll();
        } catch (DALException e) {
            e.printStackTrace();
        }
        return juries;
    }





}
