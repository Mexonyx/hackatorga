package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Jury;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOHackathon;

import java.util.List;

public class HackathonController {

    private List<Hackathon> hackathons;

    /**
     * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
     */
    private static HackathonController instanceCtrl;
    /**
     * Pattern Singleton
     * @return HackathonController
     */
    public static synchronized HackathonController getInstance(){
        if(instanceCtrl == null){
            instanceCtrl = new HackathonController();
        }
        return instanceCtrl;
    }

    /**
     * Constructeur
     * Chargement de la liste des hackathons
     * En private : pattern Singleton
     */
    private HackathonController() {

        try {
            this.hackathons = DAOHackathon.getInstance().getAll();
        } catch (DALException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de récupérer l'ensemble des hackathons
     * @return la liste des hackathons
     */
    public List<Hackathon> getAll(){
        return hackathons;
    }

    /**
     * Permet de sauvegarder les settings d'un hackathon
     * @param hackathonManaged
     * @param name nom du hacakthon
     * @param topic topic du hackathon
     * @param description description du hackathon
     */
    public void saveHackathonStandalone(Hackathon hackathonManaged, String name, String topic, String description) {

        if (hackathonManaged != null) {
            // maj hackathon existant
            hackathonManaged.setName(name);
            hackathonManaged.setTopic(topic);
            hackathonManaged.setDescription(description);
            //persistance : Update
            try {
                DAOHackathon.getInstance().updateSettings(hackathonManaged);
            } catch (DALException e) {
                e.printStackTrace();
            }
        } else {
            // Nouvel hackathon
            hackathonManaged = new Hackathon(name, topic, description);
            //persistance -> Insert
            try {
                DAOHackathon.getInstance().saveSettings(hackathonManaged);
            } catch (DALException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Permet de persister l'ensemble des jury du hackathon
     * @param hackathonManaged : hackathon
     * @param juries : liste de jury
     */
    public void saveJuriesByIdHackathon(Hackathon hackathonManaged,List<Jury> juries) {

        // maj objet
        hackathonManaged.clearJury();
        hackathonManaged.setJuryMembers(juries);

        //persistance
        try {
            //suppression des jurys stockées en BDD
            DAOHackathon.getInstance().deleteAllJuryComposeOfHackathon(hackathonManaged.getId());

            // insertion des nouveaux jurys
            for (Jury oneJury :juries) {
                DAOHackathon.getInstance().saveJury(hackathonManaged.getId(),oneJury);
            }

        } catch (DALException e) {
            e.printStackTrace();
        }
    }
}
